# Documentation

# First Steps

First steps: Set up Jelastic CLI to simplify code deployment.

Jelastic CLI works best on Mac/Linux, on Windows using the Windows Subsystem
for Linux is a usable alternative. Prerequisites:

NodeJS, NPM, Maven

For NodeJS and NPM, using [nvm](https://github.com/nvm-sh/nvm) is the easiest option to get a working NodeJS
environment which is usable with Windows.

Maven can be installed the usual way, it comes with binaries for Windows and
Linux. Just set up the %JAVA_HOME% in the `Path` Environment variables and it
should work.

Next:

- Add the Jelastic Maven Plugin to the `pom.xml` of Spring-Boot applications as
described in the
[documentation](https://docs.jelastic.com/maven-plugin-jelastic).

- Create environment with Springboot Application with the Jelastic GUI, expose Endpoint of application (eg. 8110 for customer-core)

- Build and deploy the application from the CLI using `mvn clean install jelastic:deploy`

# Deploying Lake Side Mutual

Following these steps, we can easily deploy all applications in a first test run:

## backend services 
### code adaptions
 - open the 'application.properties' inside src/main/resources
 - change the ip/port of the dependencies to the formerly noted ones

### environment creation/configuration
- create environment for spring boot
- check to use public ip
- deactivate firewall to open all ports
- upload compiled jar file to deploy it
- make note of public ip to use it in dependent projects

## frontend services
### code adaptions
 - open src/config.js
 - change ip/port of the services to the public ips/ports of the deployed nodes

### environment creation/configuration
 - create environment for node (v. 12)
 - check to use public ip
 - deactivate firewall to open all ports
 - upload zipped folder of 'module'
 - web-ssh on application
 - run 'cd ROOT'
 - run 'npm run build'
 - run 'npm install -g serve'
 - run 'serve -s build'
 - copy public ip of the deployment node and use port described after 'serve' command to access frontent service

Since this will leave us with an unsecured environment and it is hard to keep
track of any changes made to the code in order to deploy it, we will add git
repositories for all the individual projects.

# Code Management and Deployment

First we split up the main Repository (https://github.com/Microservice-API-Patterns/LakesideMutual)
on our own fork (https://gitlab.com/LKS90/LakesideMutual). This repository will
have all front- and backend applications as submodules. This repository also
contains this file as well as minor changes to the clientside application
risk-management-client.

These repositories could be made private, Jelastic allows you to add login
credentials to your account for git access. This would alleviate some concerns
with security when storing credentials in the application code, but that
should just never be done.

The Java based Springboot applications can also be built in the cloud using a
Maven build node, with the repositories described above:

![Maven Build node](./resources/mvn_deploy.JPG)

## ActiveMQ changes

While there were no problems whith the gRPC backend, ActiveMQ was complaining
in the logs. There was no documentation for this specific error, but one thread
on [stackoverflow.com](https://stackoverflow.com/questions/35845017/grails-activemq-application-doesnt-work-for-jelastic-paas)
did have a configuration which works for that user in his jelastic environment.
The only difference was `useJmX = false`, which we also added to our ActiveMQ
application. This removed the errors from the log entries.

Some research indicates that JMX allows you to monitor and control the behavior
of the ActiveMQ broker[*](https://cwiki.apache.org/confluence/display/ACTIVEMQ/JMX).
Why this causes problems is beyond us.

## Enabling Persistence:

To enable persistence, we added MySQL databases to each environment and added
the root user to the config, so the application could reach the database.
The database admin credentials are sent to the email which registered for the
cloud service provider. When trying to set up new users, since adding the root
credentials to the repository is unsecure, the default policy settings of MySQL
require you to create a user with restricted access and secure login
credentials.

The code changes to enable persistence in the Java applications:

````Java
spring.datasource.url = jdbc:mysql://node97347-customer-self-service-backend.appengine.flow.ch:3306/customerselfservice
spring.datasource.username = [USERNAME]
spring.datasource.password = [PASSWORD]
spring.datasource.driverClassName = com.mysql.jdbc.Driver
````
Switching to a MySQL JDBC Driver in the Springboot config is enough, h2 will
notice and use the MySQL endpoint instead of storing it locally (the local
driver must be removed from the application package information).
The datasource URL can be found in the jelastic documentation: https://docs.jelastic.com/database-connection

## Dealing with CORS

The next problem in a secure environment is dealing with CORS. Cross Site
Scripting is a problem for disconnected microservices like this, so we should
add proxy servers to properly identify requests between frontend and backend
servers.

The final secure environment with persistence will look like this:

![Customer-Core Environment with Database and Nginx Proxy](./resources/final_environment.JPG)

To configure this proxy, a file called mappings.xml in the tcpmaps folder has
to be configured. Here is an example for the customer-self-service-frontend:

````XML
<?xml version="1.0" encoding="UTF-8" ?>

<mappings>
	<pair frontend_port="80" backend_port="3000" description="HTTP balancing" />
	<!--pair frontend_port="825" backend_port="825" description="SMTP balancing" /-->
       	<!--pair frontend_port="110" backend_port="810" description="POP3 balancing" /-->
       	<!--pair frontend_port="143" backend_port="843" description="IMAP balancing" /-->
</mappings>
````

This would also enable us to do horizontal scaling, in case one node with vertical
scaling wouldn't suffice for the current amount of users. (https://docs.jelastic.com/load-balancing)  
The default jelastic configuration for NGINX will add default headers to make some
attacks harder, for example the HTTP Header `Access-Control-Allow-Origin:`
which is set up to automatically secure requests within the Flow subdomain.
If we would have our own domain, further set up would be reuquired.

## Remaining Problems

With the secured environment, we suddenly can not reach the nodes anymore (Only
via unsecured public IP, not via the "node url" and JELASTIC_EXPOSE configured).
With an unsecured environement or when using JELASTIC_EXPOSE in the environment
variables, it works.  
But when trying to secure the application with Proxies and
using all the automated bells and whistles Jelastic comes with, it somehow 
stops working and no log files indicate any errors. That's where the
documentation is not helpful anymore and we would have to rely on support from
our cloud provider or perhaps even the third party support in case our
application causes a problem for the Jelastic framework itself.

# Analyse: SWOT-Assessment von Cloud Provider und Cloud Offering

|                                                                           | Positiv (Nützlich) | Negativ (Schädlich)            |
| ------------------------------------------------------------------------- |:------------------ |:------------------------------ |
| Interne (innere, immanente)<br> Eigenschaften des analysierten Offerings  | **Stärken** <br>• Bombensicheres Datenzentrum <br> • ISO/IEC 27001 Zertifieziert <br> • Java weit verbreitet in existierender Infrasturktur <br> • Schneller, lokaler Support | **Schwächen** <br> • Sehr teuer im Vergleich <br> • Nur zwei Standorte, beide in der Ostschweiz <br> • Keine Meldung auf Statusseite trotzt unerreichbarkeit ||
| Externe, kontextuelle <br> Eigenschaten des analysten Offerings           | **Chancen** <br> • Weiterhin viele KMUs, welche ihre Infrastruktur auslagern wollen <br> • Viele weitere Rechenzentren in der Schweiz verfügbar <br> • Einfache Oberfläche, für Einsteiger geeignet | **Bedrohungen (Risiken)** <br> • Grosskunden könnten Jelastic selbst hosten <br> • Viel Konkurenz von grossen, aber auch kleinen Anbietern <br> • Jelastic mehraufwand könnte abschrecken bei existierender Applikation ||

# Management Summary

*Geben Sie, gestützt auf Ihre SWOT-Analyse, eine Empfehlung, ob und, wenn ja, in welchen Kontexten Ihr Cloud-Provider für das produktive Hosting von Cloud-Anwendungen im Sinne der Cloud User Stories und Cloud Use Cases aus der
ersten Vorlesungslektion in Frage kommt.* 

## Cloud User Stories

**Cloud User Story 1:**  
Cloud-Native App. (Lean Startup Developer)

Schnelle Antwort: Nein. Um Flow zu rechfertigen braucht es einige Kriterien.
Falls man eine Java basierte Applikation aufbauen möchte, welche unbedingt die
Sicherheit eines atombombensicheren Rechenzentrums braucht, dann wäre Flow eine
gute Option. Ansonsten gäbe es günstigere Jelastic Anbieter, und dann ist da
immernoch die Frage, ob man sich in Jelastic und die Skalierungsoptionen welche
darin integriert sind einarbeiten möchte. Vendor Lock-In wäre hier schon ein
Problem, welches man als Startup bedenken sollte.

**Cloud User Story 2:**
Cloud Migration (Application Maintainer)

Auch hier würde man eher davon abraten, es sei denn die Gegebenheiten erfordern
Jelastic & Bombensicherheit.  
Wenn alle existierenden Applikationen vom Jelastic Stack unterstützt werden und
das Unternehmen die bereits genannte Datensicherheit benötigt, dann könnte man
einen Umzug durchaus einmal evaluieren. Allerdings war unsere Evaluierung trotz
der Unterstützung aller Applikationen auf den ersten Blick erfolgreich, jedoch
der mehraufwand in der Administration bei einem sicheren Betrieb in der Cloud
sollte nicht unterschätzt werden. Ob dies von Jelastic abhängig ist, konnte
hier nicht beurteilt werden.  

## Cloud Use Cases

- Make data available to large communities

Da man nicht für Traffic berechnet wird, wäre es eine Option. Allerdings gäbe
es bestimmt auch hier wieder günstigere Varianten

- Perform computation-intense activities

Jelastic bietet keinen Nativen support für HPC Aufgaben, mit einigem Aufwand
könnte man mit Docker Containern warscheinlich eine Lösung finden. Allerdings
wäre es mit sicherheit sehr teuer, mit den ohnehin hohen Preisen und der
Bezahlung je nach Nutzung. 

- One-time usage

Falls man mit Jelastic vertraut ist und die Sicherheit von Flow benötigt, dann
ja. Allerdings kostet es bestimmt mehr als andere Optionen.

- Support for short-term projects

Ähnlich der One-Time usage, wobei bezweifelt werden kann, warum ein Projekt in
einem Atombunker gehostet werden müsste.

- Low-risk startup/scaled agile

Zwar minimiert der Bunker risiken, Jelastic und hohe Kosten sind jedoch nicht
sehr förderlich. Daher ein klares Nein. 

- Backup

Eindeutig eine Option, allerdings wäre ein Angebot ohne das PaaS Frontend mit
Sicherheit günstiger. Falls eine existierende Backup Lösung existiert, welche
mit Jelastic kombiniert werden könnte, wäre es eine gute Option.

- Disaster recovery

Auch hier, eine gute Option da Bombensicher. Allerdings auch wieder die Frage,
warum ein PaaS System wie Jelastic nötig sein sollte. 

- General-purpose software

Aufgrund des Preises ein Nein. Es gäbe andere Jelastic Anbieter, oder man
verzichtet ganz auf Jelastic.

